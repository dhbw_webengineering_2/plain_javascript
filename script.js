function showDadJoke() {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        const joke = JSON.parse(xhttp.response);
        document.getElementById('joke').innerHTML = joke.joke;
    }
    xhttp.open('GET', 'https://icanhazdadjoke.com/', true);
    xhttp.setRequestHeader('Accept', 'application/json');
    xhttp.send();
}